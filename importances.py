# usage: python importances.py <dataset>

from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import RandomForestRegressor
from sklearn.feature_selection import SelectFromModel
from set_from_csv import set_from_csv
from sys import argv

if len(argv) == 1:
	print ("PLEASE SPECIFY THE DATASET")
	exit()
filename = "dataset/"+argv[1]
try:
	myset = set_from_csv(filename)
except:
	print ("WHAT?")
	exit()

TESTSIZE = 100
X = [arr[1:] for arr in myset[:-TESTSIZE]]
Y = [arr[0] for arr in myset[:-TESTSIZE]] # predicted value

clf = RandomForestClassifier() 
if "pop" in filename:
	clf = RandomForestRegressor()
clf = clf.fit(X, Y)
importances = clf.feature_importances_
print(importances)
