# usage: predict_test <genre/pop/date> <optional:testsize> <optional:resultfile>
# testsize defaults to 100
# resultfile stores {pred:true} pairs for later use
# the script outputs prediction results and accuracy

from sklearn import svm
from sklearn.externals import joblib
from sklearn.metrics import accuracy_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import median_absolute_error
from set_from_csv import set_from_csv
from random import shuffle
from sys import argv

DATADIR = "dataset/"
MODELDIR = "model/"

type_name = {
	"genre" : "trimmed_1.pkl",\
	"pop" : "trimmed_2.pkl",\
	"date" : "trimmed_3.pkl",\
}

if len(argv) == 1:
	print ("PLEASE SPECIFY THE TASK")
	exit()
prediction_type = argv[1]
if prediction_type not in type_name.keys():
	print ("WHAT?")
	exit()

clf = joblib.load(MODELDIR+type_name[prediction_type])
data = set_from_csv(DATADIR+type_name[prediction_type][:-3]+"csv")
shuffle(data)

TESTSIZE = 100
if len(argv) > 2:
	TESTSIZE = int(argv[2])

target = [arr[1:] for arr in data[-TESTSIZE:]] # predicted value
result = clf.predict(target)

# display results
print ("PRED\tTRUE\n----\t----")
for i in range(TESTSIZE):
	print ("%d\t%d" % (result[-i], data[-i][0]))
print ("----\t----")
# display accuracy
if prediction_type == "pop":
	# print ("MEAN SQUARE ERROR:", mean_squared_error(result, [line[0] for line in data[-TESTSIZE:]]))
	print ("MEAN AE\t\t", mean_absolute_error(result, [x[0] for x in data[-TESTSIZE:]]))
	print ("MEAN SQ E \t", mean_squared_error(result, [x[0] for x in data[-TESTSIZE:]]))
	print ("MEDIAN AE\t", median_absolute_error(result, [x[0] for x in data[-TESTSIZE:]]))

else:
	right = sum([1 for i in range(TESTSIZE) if result[-i] == data[-i][0]])
	print ("ACCURACY: %d%%" % (accuracy_score(result, [x[0] for x in data[-TESTSIZE:]])*100))

if len(argv) > 3:
	open(argv[3], "w").write("\n".join(["%d;%d" % (result[-i], data[-i][0]) for i in range(TESTSIZE)]))
