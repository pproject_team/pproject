import re
import os

def renaming_files(path):
    n = 1
    for root, dirs, files in os.walk(path):
        for filename in os.listdir(path):
            os.rename(path + filename, path + SITE + str(n) + ".txt")
            n += 1

def collect_data(path):
    csvfile = open(SITE + '.csv', 'w', encoding='utf8')

    for root, dirs, files in os.walk(path):
        for filename in files:
            f = open(path + filename, 'r')
            htmltext = f.read()

            if site == "shansoninfo":
                re_songname = re.compile('<meta content="  (.+?)" name="description" />')
                
                title = re.findall(re_songname, htmltext)[0].split(" &#8212; ")
                if len(title) < 2:
                    title = title[0].split(" – ")
                artist = title[0]
                if len(title) < 2:
                    songname = ""
                else:
                    songname = title[1]

            elif site == "shansontext":
                re_songname = re.compile('<title>(.+?)</title>')
                
                title = re.findall(re_songname, htmltext)[0].split(" - ")
                artist = title[0]
                songname = title[1]

            data = "@".join([filename, artist, songname]) + "@@@@"
            csvfile.write(data + '\n')

    return csvfile

def clear_texts(path):
    for root, dirs, files in os.walk(path):
        for filename in files:
            if filename.endswith('.txt'):
                f = open(path + filename, 'r')
                htmltext = f.read().replace('\n', '').replace('\r', '')

                if SITE == "shansoninfo":
                    re_songtext = re.compile('<div class="entry">(.+?)<div class="clear">', flags = re.DOTALL)
                    re_tags = re.compile('\s*<.+?>\s*')
                
                    songtext = re.findall(re_songtext, htmltext)[0]
                    songtext = re.sub(re_tags, '\n', songtext)
                    accords = ["/?Am", "/?Dm", "/?G", "/?C", "/?E", "/?A?7", "/?A", "/?G"]
                    for each in accords:
                        songtext = songtext.replace(each, "").replace(each.lower(), "")


                if SITE == "shansontext":
                    re_songtext = re.compile('</h2>(.+?)<!-- Put', flags = re.DOTALL)
                    re_tags = re.compile('\s*<.+?>\s*')
                
                    songtext = re.findall(re_songtext, htmltext)[0]
                    songtext = re.sub(re_tags, '\n', songtext)

                f = open(path + filename, 'w', encoding="utf8")
                f.write(songtext)

SITE = input() ## could be "shansoninfo" or "shansontext"
path = "C:/песни/shanson-text.ru/" ## directory with downloaded site (by wget)
renaming_files(path) ## renaming html(or php)-files to txt-files, format "site+n.txt", where n - number of file
collect_data(path) ## collecting data about song to csv-file
clear_texts(path) ## collecting text from html, clearing it from tags and chords