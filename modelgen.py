# usage: python modelgen.py <target_csv_file> <randomstuff>
# <randomstuff> lets the script know we're dealing with pop (i.e. regression)
# generated model keeps the target's name with .pkl extension

from sys import argv
from set_from_csv import set_from_csv
from sklearn.externals import joblib
from sklearn import cross_validation
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import VotingClassifier
from sklearn.grid_search import GridSearchCV
from sklearn.kernel_ridge import KernelRidge

DATADIR = "dataset/"
MODELDIR = "model/"
TESTSIZE = 100

name = argv[1]
dataset = set_from_csv(DATADIR+name)

# enter random 3rd argument to let the script know we're dealing with pop
if len(argv) > 2:
	# pop
	clf = KernelRidge()
else:
	# genre, date
	clf1 = LogisticRegression(random_state=1) 
	clf2 = RandomForestClassifier(random_state=1) 
	clf3 = GaussianNB() 
	eclf = VotingClassifier(estimators=[('lr', clf1), ('rf', clf2), ('gnb', clf3)], voting='hard') 
	params = {} 
	clf = GridSearchCV(estimator=eclf, param_grid=params) 

data = [arr[1:] for arr in dataset[:-TESTSIZE]]
target = [arr[0] for arr in dataset[:-TESTSIZE]] # predicted value

clf.fit(data, target)
joblib.dump(clf, MODELDIR+name[:-3]+"pkl")
