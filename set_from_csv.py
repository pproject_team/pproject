def set_from_csv(csvfile):
	dataset = []

	read_csv = open(csvfile)
	for string in read_csv.read().split("\n")[1:]:
		dataset.append([float(a) for a in string.split(';')])

	return dataset
