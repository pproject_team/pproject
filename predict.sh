FILENAME=$1
TYPE=$2
if [ "$#" -ne 2 ]; then
	echo "usage: $0 textfile genre/pop/date"
	exit
fi

# echo "./stemtext.sh $1"
./stemtext.sh $1
# echo "python3 getfeatures.py tagged_$1 > features_$1"
python3 getfeatures.py tagged_$1 > features_$1
# echo "python3 predict.py features_$1 $2"
python3 predict.py features_$1 $2

rm tagged_$1
rm features_$1