#aaa

import os
import re

address = 'muzoton.ru/tags'
data = open('data.csv', 'w', encoding='utf8')

re_artistname = re.compile('<h1>Тексты песен (.+?)</h1>')
re_songs = re.compile('<div class="cell cellsong"><a href="(.+?)">.+? [-–] (.+?)</a></div>\s+?<div class="cell cellrate">(.+?)</div>')
re_lyrics = re.compile('<div class="songtext">(.+?)</div>', flags = re.DOTALL)
re_genre = re.compile('<div class="genre">.+?<a href="http://muzoton.ru/.+?">(.+?)</a>', flags = re.DOTALL)
re_removelinks = re.compile('.+href.+\n')

songfilenumber = 0

# walking through all the files in main dir
for a, b, c in os.walk(address):
	for filename in c:

		# only index.html pages are required
		if filename == 'index.html':
			
			f = open(a + '/' + filename, 'r', encoding='cp1251').read()

			# trying to find artistname, skipping on fail
			artist = re.findall(re_artistname, f)
			if artist == []:
				continue

			artist = artist[0]

			# parsing a list of songs by artist
			songs = re.findall(re_songs, f)
			
			for song in songs:

				try: # will elaborate on that later

					# increasing songfile number and updating songfile name
					songfilenumber += 1
					songfilename = 'songtext_%d' % songfilenumber

					# opening file to parse song data
					songfile = open(song[0][7:], 'r', encoding='cp1251').read()

					songgenre = re.findall(re_genre, songfile)[0]

					# updating table with song data
					songstring = '%s@%s@%s@%s@%s@%s\n' % (song[0][7:], songfilename, artist, song[1], song[2], songgenre)
					data.write(songstring)

					# parsing song lyrics
					lyrics = re.findall(re_lyrics, songfile)[0]
					lyrics = re.sub('<.+?>', '\n', lyrics)
					lyrics = re.sub('\n{2,}', '\n', lyrics)
					lyrics = re.sub('\t', '', lyrics)
					lyrics = re.sub(re_removelinks, '', lyrics)
					
					# storing song info and lyrics
					lyrics_file = open('songs/' + songfilename, 'w', encoding='utf8')
					lyrics_file.write(songstring + lyrics[1:])
					lyrics_file.close()

				except:
					pass

# done
data.close()
