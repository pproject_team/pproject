# usage: predict_test <genre/pop/date> <optional:testsize> <optional:resultfile>
# testsize defaults to 100
# resultfile stores {pred:true} pairs for later use
# the script outputs prediction results and accuracy

from sklearn import svm
from sklearn.metrics import mean_squared_error
from sklearn.externals import joblib
from set_from_csv import set_from_csv
from random import shuffle
from sys import argv

type_name = {
	"genre" : "trimmed_1.pkl",\
	"pop" : "trimmed_2.pkl",\
	"date" : "trimmed_3.pkl",\
}

genres = {
	1 : "pop",\
	2 : "rock",\
	3 : "rap",\
	4 : "chanson",\
}

dates = {
	1 : "-1988",\
	2 : "1992-1999",\
	3 : "2003-",\
}

DATADIR = "dataset/"
MODELDIR = "model/"

features = [float(value) for value in open(argv[1]).read().split(";")]
prediction_type = argv[2]
if prediction_type not in type_name.keys():
	print ("WHAT?")
	exit()

clf = joblib.load(MODELDIR+type_name[prediction_type])
result = clf.predict([features])[0]

if prediction_type == "genre":
	result = genres[result]
elif prediction_type == "pop":
	result = str(result)
elif prediction_type == "date":
	result = dates[result]

print (result)