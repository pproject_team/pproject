import os
import re

ENDSTRING = 'tekst-pesni'
songs_dir = 'texts2/'
csv = open('songs2.csv', 'w', encoding='utf8')
songfile_template = songs_dir + 'rapnax%d'

re_songname = re.compile('<title>(.+?) [-–] (.+?)</title>')
re_songtext = re.compile('<table class="contentpaneopen">(.+?)<script', flags = re.DOTALL)
re_cutstart = re.compile('.+<td valign="top">', flags = re.DOTALL)
re_cutstart = re.compile('.+\(текст песни\)</b><br />\n<br />\n', flags = re.DOTALL)
re_tags = re.compile('\s*<.+?>\s*')
re_popularity = re.compile('class="like l-vk">.+?<i class="l-ico"></i>.+?<span class="l-count">(.?)</span>', flags = re.DOTALL)

songnumber = 1

for a, b, c in os.walk('.'):
	for filename in c:
		if filename[-11:] == ENDSTRING:

			address = '%s/%s' % (a, filename)
			html = open(address, 'r', encoding='utf8')
			html_text = html.read()

			namestring = re.findall(re_songname, html_text)[0]
			artist = namestring[0]
			songname = namestring[1][:-14]

			songtext = re.findall(re_songtext, html_text)[0]
			songtext = re.sub(re_cutstart, '', songtext)
			songtext = re.sub(re_tags, '\n', songtext)

			popularity = re.findall(re_popularity, html_text)[0]

			text_filename = songfile_template % songnumber
			docstring = "%s@%s@%s@@%s@@\n" % ('rapnax%d' % songnumber, artist, songname, popularity)
			songnumber += 1
			text_file = open(text_filename, 'w', encoding = 'utf8')

			csv.write(docstring)
			text_file.write(docstring+songtext)
			text_file.close()
			print(artist, songname)
csv.close()