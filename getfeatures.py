#coding:utf8
# usage: python getfeatures.py <taggedtext_name>
# results are ordered the following way:

# lines - how many lines does the text contain
# obscene - percentage of swear words and such
# past_tense_ratio - percentage of words in past tense
# pos_ratio_A - percentage of adjectives
# pos_ratio_ADV - percentage of adverbs
# pos_ratio_CONJ - percentage of conjunctions
# pos_ratio_PRO - percentage of pronouns
# pos_ratio_S - percentage of nouns
# pos_ratio_V - percentage of verbs
# typetoken - unique/all words
# words_per_line - how many words does an average line contain

import os
import re
from collections import OrderedDict

def getfeatures(text):

	text = re.sub("\n{2,}", "\n", text)
	features = OrderedDict()

	word_count = 0
	pos_tags = {
	"S":	0,
	"V":	0,
	"A":	0,
	"ADV":	0,
	"CONJ":	0,
	"APRO":	0,
	"SPRO":	0
	}

	# wordcount and pos_tags (not features, used later to compute them though)
	for tag in re.findall("{[а-я]+=([A-Z]+)", text):
		word_count += 1
		if tag in pos_tags:
			pos_tags[tag] += 1
		else:
			pos_tags[tag] = 1

	# lines
	features["lines"] = len(re.findall("\n", text))

	# obscene
	try:
		features["obscene"] = len(re.findall(",обсц", text))/word_count
	except ZeroDivisionError:
		features["obscene"] = 0

	# past_tense_ratio
	try:
		features["past_tense_ratio"] = len(re.findall("=прош", text))/len(re.findall("=V[=,]", text))
	except ZeroDivisionError:
		features["past_tense_ratio"] = 0

	# ratios
	try:
		features["pos_ratio_A"] = pos_tags["A"]/word_count
	except ZeroDivisionError:
		features["pos_ratio_A"] = 0

	try:
		features["pos_ratio_ADV"] = pos_tags["ADV"]/word_count
	except ZeroDivisionError:
		features["pos_ratio_ADV"] = 0

	try:
		features["pos_ratio_CONJ"] = pos_tags["CONJ"]/word_count
	except ZeroDivisionError:
		features["pos_ratio_CONJ"] = 0

	try:
		features["pos_ratio_PRO"] = (pos_tags["APRO"]+pos_tags["SPRO"])/word_count
	except ZeroDivisionError:
		features["pos_ratio_PRO"] = 0

	try:
		features["pos_ratio_S"] = pos_tags["S"]/word_count
	except ZeroDivisionError:
		features["pos_ratio_S"] = 0

	try:
		features["pos_ratio_V"] = pos_tags["V"]/word_count
	except ZeroDivisionError:
		features["pos_ratio_V"] = 0

	# type-token ratio
	lemmas = re.findall("{(\w+)", text)
	unique_lemmas = []
	for lemma in lemmas:
		if lemma not in unique_lemmas:
			unique_lemmas.append(lemma)	

	try:
		features["typetoken"] = len(unique_lemmas)/len(lemmas)
	except ZeroDivisionError:
		features["typetoken"] = 0

	# words per line
	try:
		features["words_per_line"] = len(lemmas)/features["lines"]
	except ZeroDivisionError:
		features["words_per_line"] = 0

	line = [str(features[feature]) for feature in sorted(features)]
	# print (len(text))
	return line

if __name__ == "__main__":
	import sys
	text = open(sys.argv[1], "r", encoding="utf8").read()
	print (";".join([str(feature) for feature in getfeatures(text)]))
